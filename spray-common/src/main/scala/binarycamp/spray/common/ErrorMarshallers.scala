package binarycamp.spray.common

import spray.httpx.marshalling.{ Marshaller, ToResponseMarshaller }
import DefaultHttpHeaders.NoCacheHeaders
import ErrorToStatusCodeMapper.Default

trait ErrorMarshallers {
  implicit def errorMarshaller[T](implicit translator: ErrorTranslator[T],
                                  statusCodeMapper: ErrorToStatusCodeMapper,
                                  m: Marshaller[T]): ToResponseMarshaller[Error] =
    ToResponseMarshaller.fromStatusCodeAndHeadersAndT.compose {
      error ⇒ (statusCodeMapper.orElse(Default)(error), NoCacheHeaders, translator(error))
    }

  implicit def errorLocalizableMarshaller[T](implicit translator: ErrorTranslator[T],
                                             statusCodeMapper: ErrorToStatusCodeMapper,
                                             m: Marshaller[T]): LocalizableMarshaller[Error] =
    LocalizableMarshaller[Error] { languages ⇒
      ToResponseMarshaller.fromStatusCodeAndHeadersAndT.compose {
        error ⇒ (statusCodeMapper.orElse(Default)(error), NoCacheHeaders, translator(error, languages))
      }
    }
}

object ErrorMarshallers extends ErrorMarshallers
