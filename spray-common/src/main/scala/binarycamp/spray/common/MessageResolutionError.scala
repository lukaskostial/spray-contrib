package binarycamp.spray.common

import java.util.Locale

sealed trait MessageResolutionError

object MessageResolutionErrors {
  case class ResolutionFailed(message: String, cause: Throwable) extends MessageResolutionError
  case class MessageNotFound(code: String, locales: Seq[Locale]) extends MessageResolutionError
}
