package binarycamp.spray.common

import scala.concurrent.{ ExecutionContext, Future }
import scala.util.{ Failure, Success, Try }
import spray.http.StatusCodes.NotFound
import spray.http.{ HttpResponse, StatusCode }
import spray.httpx.marshalling.ToResponseMarshaller

trait StatusCodeMarshallers extends LowPriorityStatusCodeMarshallers {
  implicit def statusCodeAndOptionMarshaller[S, T](implicit m: ToResponseMarshaller[(S, T)],
                                                   s: S ⇒ StatusCode): ToResponseMarshaller[(S, Option[T])] =
    ToResponseMarshaller[(S, Option[T])] {
      case ((sc, Some(t)), ctx) ⇒ m(sc → t, ctx)
      case ((_, None), ctx)     ⇒ ctx.marshalTo(HttpResponse(NotFound))
    }

  implicit def statusCodeAndEitherMarshaller[S, A, B](implicit ma: ToResponseMarshaller[A],
                                                      mb: ToResponseMarshaller[(S, B)],
                                                      s: S ⇒ StatusCode): ToResponseMarshaller[(S, Either[A, B])] =
    ToResponseMarshaller[(S, Either[A, B])] {
      case ((_, Left(a)), ctx)   ⇒ ma(a, ctx)
      case ((sc, Right(b)), ctx) ⇒ mb(sc → b, ctx)
    }

  implicit def statusCodeAndFutureMarshaller[S, T](implicit m: ToResponseMarshaller[(S, T)],
                                                   ec: ExecutionContext,
                                                   s: S ⇒ StatusCode): ToResponseMarshaller[(S, Future[T])] =
    ToResponseMarshaller[(S, Future[T])] {
      case ((sc, f), ctx) ⇒ f onComplete {
        case Success(v)     ⇒ m(sc → v, ctx)
        case Failure(error) ⇒ ctx.handleError(error)
      }
    }

  implicit def statusCodeAndTryMarshaller[S, T](implicit m: ToResponseMarshaller[(S, T)],
                                                s: S ⇒ StatusCode): ToResponseMarshaller[(S, Try[T])] =
    ToResponseMarshaller[(S, Try[T])] {
      case ((sc, Success(v)), ctx) ⇒ m(sc → v, ctx)
      case ((_, Failure(t)), ctx)  ⇒ ctx.handleError(t)
    }
}

trait LowPriorityStatusCodeMarshallers {
  implicit def forStatusCodeAndT[S, T](implicit m: ToResponseMarshaller[T],
                                       s: S ⇒ StatusCode): ToResponseMarshaller[(S, T)] =
    ToResponseMarshaller[(S, T)] {
      case ((sc, t), ctx) ⇒ m(t, ctx.withResponseMapped(_.copy(status = sc)))
    }
}
