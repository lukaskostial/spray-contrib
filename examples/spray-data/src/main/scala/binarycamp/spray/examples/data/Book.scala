package binarycamp.spray.examples.data

case class Book(id: String, name: String, author: String)

case class NewBook(name: String, author: String)
