package binarycamp.spray.session

import spray.routing.Rejection

case class MalformedSessionRejection(error: String, cause: Option[Throwable]) extends Rejection

object MalformedSessionRejection {
  def apply(error: String): MalformedSessionRejection = MalformedSessionRejection(error, None)

  def apply(error: String, cause: Throwable): MalformedSessionRejection = MalformedSessionRejection(error, Some(cause))
}

case class SessionEncodingFailedRejection(error: String, cause: Option[Throwable]) extends Rejection

object SessionEncodingFailedRejection {
  def apply(error: String): SessionEncodingFailedRejection = SessionEncodingFailedRejection(error, None)

  def apply(error: String, cause: Throwable): SessionEncodingFailedRejection =
    SessionEncodingFailedRejection(error, Some(cause))
}

case class MissingSessionAttributeRejection(attribute: String) extends Rejection

case class MalformedSessionAttributeRejection(attribute: String, error: String, cause: Option[Throwable]) extends Rejection

object MalformedSessionAttributeRejection {
  def apply(attribute: String, error: String): MalformedSessionAttributeRejection =
    MalformedSessionAttributeRejection(attribute, error, None)

  def apply(attribute: String, error: String, cause: Throwable): MalformedSessionAttributeRejection =
    MalformedSessionAttributeRejection(attribute, error, Some(cause))
}
