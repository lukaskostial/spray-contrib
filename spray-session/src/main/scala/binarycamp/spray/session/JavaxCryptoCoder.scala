package binarycamp.spray.session

import com.typesafe.config.Config
import javax.crypto._
import javax.crypto.spec.{ PBEKeySpec, SecretKeySpec }
import org.apache.commons.codec.binary.Base64
import spray.http.Uri.Query
import spray.util.UTF8

class JavaxCryptoCoderConfigurator(config: Option[Config]) extends ContentCoderConfigurator(config) {
  override def create: ContentCoder = config match {
    case Some(c) ⇒ new JavaxCryptoCoder(
      c.getString("algorithm"),
      c.getString("transformation"),
      c.getString("secret"),
      c.getString("secret-key-factory-algorithm"),
      c.getInt("iteration-count"),
      c.getInt("key-length"))
    case None ⇒ throw new IllegalArgumentException("JavaxCryptoCoderConfigurator requires a non-empty config")
  }
}

class JavaxCryptoCoder(algorithm: String, transformation: String, secret: String, secretKeyFactoryAlgorithm: String,
                       iterationCount: Int, keyLength: Int) extends ContentCoder {
  private val salt = Array[Byte] { 0 }

  private val key = {
    val keySpec = new PBEKeySpec(secret.toCharArray, salt, iterationCount, keyLength)
    val secretKey = SecretKeyFactory.getInstance(secretKeyFactoryAlgorithm).generateSecret(keySpec)
    new SecretKeySpec(secretKey.getEncoded, algorithm)
  }

  override def encode(session: Session): String =
    Base64.encodeBase64String(encrypt(Query(session).toString().getBytes(UTF8)))

  override def decode(content: String): Session =
    Query(new String(decrypt(Base64.decodeBase64(content)), UTF8)).toMap

  private def encrypt(data: Array[Byte]): Array[Byte] = doFinal(data, Cipher.ENCRYPT_MODE)

  private def decrypt(data: Array[Byte]): Array[Byte] = doFinal(data, Cipher.DECRYPT_MODE)

  private def doFinal(data: Array[Byte], mode: Int): Array[Byte] = {
    val cipher = Cipher.getInstance(transformation)
    cipher.init(mode, key)
    cipher.doFinal(data)
  }
}
