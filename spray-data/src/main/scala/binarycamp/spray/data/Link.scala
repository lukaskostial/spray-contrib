package binarycamp.spray.data

import spray.http.Uri

case class Link(relation: String, href: Uri, title: Option[String]) {
  def isSelf: Boolean = relation == Link.Self
}

object Link {
  val Self = "self"
  val Prev = "prev"
  val Next = "next"

  def apply(href: Uri): Link = apply(Self, href)

  def apply(href: Uri, title: String): Link = apply(Self, href, title)

  def apply(relation: String, href: Uri): Link = Link(relation, href, None)

  def apply(relation: String, href: Uri, title: String): Link = Link(relation, href, Some(title))
}
