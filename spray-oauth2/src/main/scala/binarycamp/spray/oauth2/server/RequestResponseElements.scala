package binarycamp.spray.oauth2.server

object RequestResponseElements {
  // format: OFF
  val ResponseType     = "response_type"
  val ClientId         = "client_id"
  val RedirectionUri   = "redirect_uri"
  val ScopeParam       = "scope"
  val State            = "state"
  val GrantType        = "grant_type"
  val ClientSecret     = "client_secret"
  val AccessToken      = "access_token"
  val TokenType        = "token_type"
  val ExpiresIn        = "expires_in"
  val RefreshToken     = "refresh_token"
  val Username         = "username"
  val Password         = "password"
  val ErrorCode        = "error"
  val ErrorDescription = "error_description"
  val ErrorUri         = "error_uri"
  // format: ON
}
