package binarycamp.spray.oauth2.server

import binarycamp.spray.common.ParamMapWriter
import binarycamp.spray.oauth2.server.RequestResponseElements.State
import binarycamp.spray.pimp._
import spray.http.StatusCodes.{ Found, Redirection }
import spray.http.Uri
import spray.routing.Directives.redirect
import spray.routing.Route

trait ResponseSender {
  def sendInQuery[A](response: A, uri: Uri, state: Option[String],
                     statusCode: Redirection = Found)(implicit w: ParamMapWriter[A]): Route =
    redirect(uri.withQuery(w(response) +? (State -> state)), statusCode)

  def sendInFragment[A](response: A, uri: Uri, state: Option[String],
                        statusCode: Redirection = Found)(implicit w: ParamMapWriter[A]): Route =
    redirect(uri.withFragment(w(response) +? (State -> state)), statusCode)
}

object ResponseSender extends ResponseSender
