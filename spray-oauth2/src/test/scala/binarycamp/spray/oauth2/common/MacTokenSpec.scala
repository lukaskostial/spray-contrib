package binarycamp.spray.oauth2.common

import org.scalatest.FlatSpec
import org.scalatest.Matchers._

class MacTokenSpec extends FlatSpec {
  val t1 = MacToken("token", "key", "alg")
  val t2 = AccessToken("mac", "token", Some(Map("mac_key" -> "key", "mac_algorithm" -> "alg")))
  val t3 = AccessToken("mac", "token", Some(Map("mac_key" -> "key", "mac_algorithm" -> "alg", "a" -> "b")))
  val t4 = AccessToken("mac", "token", Some(Map("mac_key" -> "key")))
  val t5 = AccessToken("mac", "token", Some(Map("mac_algorithm" -> "alg")))
  val t6 = AccessToken("mac", "token", Some(Map.empty[String, String]))
  val t7 = AccessToken("mac", "token", None)
  val t8 = AccessToken("OtherTokenType", "token", Some(Map("mac_key" -> "key", "mac_algorithm" -> "alg")))

  "MacToken" should "equal to an AccessToken with tokenType=mac, mac_key and mac_algorithm additional attributes and the same token" in {
    t1 should equal(t2)
    t1 should not equal (t3)
    t1 should not equal (t4)
    t1 should not equal (t5)
    t1 should not equal (t6)
    t1 should not equal (t7)
    t1 should not equal (t8)
  }

  it should "extract only from AccessTokens with tokenType=mac and mac_key and mac_algorithm additional attributes" in {
    MacToken.unapply(t1) should be(Some("token", "key", "alg"))
    MacToken.unapply(t2) should be(Some("token", "key", "alg"))
    MacToken.unapply(t3) should be(None)
    MacToken.unapply(t4) should be(None)
    MacToken.unapply(t5) should be(None)
    MacToken.unapply(t6) should be(None)
    MacToken.unapply(t7) should be(None)
    MacToken.unapply(t8) should be(None)
  }
}
